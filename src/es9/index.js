const obj = {
    name: 'oscar',
    age: 32,
    country: 'MX'
};

let {country, ...all } = obj;

console.log(all);

const obj = {
    name: 'Hector',
    age: 26
}

const obj1 = {
    ...obj,
    country: 'MX'
}

console.log(obj1);

const helloWorld = () => {
    return new Promise((resolve, reject) => {
        (true)
        ? setTimeout(() => resolve('Hello World'), 3000)
        : reject(new Error('Test Error'))
    });
}
helloWorld()
    .then(response => console.log(response))
    .catch(error => console.log(error))
    .finally(() => console.log('Finalizo'))

const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/
const match = regexData.exec('2018-04020');
const year